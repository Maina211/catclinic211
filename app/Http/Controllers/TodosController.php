<?php

namespace App\Http\Controllers;
use App\numbercount;
use App\User;
use App\Todo;
use Illuminate\Http\Request;

class TodosController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $todos = Todo::OrderBy('queue_id','asc')->get();
        $users = User::find(1)->get();
        $num = numbercount::find(1);
        $num->homepage_count += 1;
        $num->sum_count += 1;
        $num->save();
        return view('welcome')->with('todos',$todos);

    }


    public  function search(Request $request)
    {
            $search = $request->get('search');
            $users = User::find(1)->get();
            if($search != ''){
                $todos = Todo::where('name_cat','like','%'.$search.'%')
                    ->orWhere('id','like','%'.$search.'%')
                    ->paginate(3)
                    ->setpath('');
                $todos->appends(array(
                    'search' => $request->get('search')
                ));
                if(count($todos)>0){
                    $num = numbercount::find(1);
                    $num->searchcount += 1;
                    $num->save();
                    return view('welcome')->with('todos',$todos);
                }
            }
            return redirect('/home')->with('notfound','NO result');

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $num = numbercount::find(1);
        $num->queuepage_count += 1;
        $num->sum_count += 1;
        $num->save();
        return  view('queue');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $this->validate($request,
                    [

                        'name_cat'=>'required',
                        'color_cat'=>'required',
                        'sex'=>'required',
                        'service'=>'required',
                       'file'=>'required|mimes:jpg,jpeg,png,gif|max:2048'
                    ]
                );
         $file_name = time().'.'.$request->file->extension();
          $request->file->move(public_path('uploads'),$file_name);
               ($request->file());
                $todo = new Todo();
                $todo->queue_id += 1;
                $todo->name_cat =  $request->input('name_cat');
               $todo->color_cat = $request->input('color_cat');
               $todo->sex = $request->input('sex');
               $todo->service = $request->input('service');
               $todo->file_name = $file_name;
                $todo->save();
                $todo->queue_id += 1;
                        $num = numbercount::find(1);
                        $num->queuecount += 1;
                        $num->sum_count += 1;
                        $num->save();
                return  redirect('/home')->with('success','ลงแล้วจ้าาาาาา');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $todo = Todo::find($id);
        return view('delete')->with('todo',$todo);
    }
    public function show2()
    {
         $todos = Todo::OrderBy('queue_id','asc')->limit(1)->get();
        return view('searchmain')->with('todos',$todos);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $todo = Todo::find($id);
        return view('edit')->with('todo',$todo);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $todo = Todo::find($id);
        $todo->name_cat =  $request->input('name_cat');
        $todo->color_cat = $request->input('color_cat');
        $todo->sex = $request->input('sex');
        $todo->service = $request->input('service');
        $todo->save();
        return redirect('/')->with('success','Edit Success');

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $todo = Todo::find($id);
        $todo->delete();
        return redirect('/home')->with('success','Deleted Success ');
    }
    public function aboutit()
    {
        $abouts = numbercount::find(1)->get();
        return view('About')->with('todos',$abouts);
    }

}
