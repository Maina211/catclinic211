<?php $__env->startSection('content'); ?>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#" ><h1>Logo</h1></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup " >
            <div class="navbar-nav" style="margin-left: 300px;">

                <a class="nav-item nav-link active" href=<?php echo e(url('/home/')); ?>><h5>หน้าแรก</h5> <span class="sr-only">(current)</span></a>
                <a class="nav-item nav-link" href=<?php echo e(url('/home/queue/')); ?>><h5>จองคิว</h5></a>
                <a class="nav-item nav-link" href="http://www.catclinic.local/search"><h5>ค้นหา</h5></a>
                <a class="nav-item nav-link" href=<?php echo e(url('/abouts')); ?>><h5>เกี่ยวกับเรา</h5></a>
            </div>
        </div>
    </nav>
    
    <form action="/search" method="get">
        <div class="container" >

            <div style="padding: 30px;">
                <div class="row"style="background: #999999; padding: 50px 0 50px  0; text-align: center;" >
                    <h5 style="margin:  0 auto">ค้นหา</h5>


                            <div class="input-group flex-nowrap" style="margin: 5px 300px 5px 300px ;" >
                                <input  type="search" name="search" placeholder="ใส่คิวหรือชื่อแมว" aria-label="Username" aria-describedby="addon-wrapping " style="width: 400px;">
                                <span class="from-group-btn">

                                    <button class="btn btn-primary"> <a style="color: white;font-size: 16px" href=<?php echo e(url('/searchtext')); ?>>ค้นหา</a></button>
                                </span>
                            </div>
                    <div class="container">
                        <h1>Laravel 5.7 Autocomplete Search using Bootstrap Typeahead JS - ItSolutionStuff.com</h1>
                        <input class="typeahead form-control" type="text">
                    </div>


                </div>
            </div>
        </div>
    </form>



    <div class="container" style="background : #222 ;color: white; height: 50px; text-align: center ;padding: 10px;" >
        <div class="row ">
            <div class="col">
                <h5>โปรเจค 2562/2 เอกเว็บและสื่อโต้ตอบ นายอภิชาติ เปรมใจ</h5>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var path = "<?php echo e(route('autocomplete')); ?>";
        $('input.typeahead').typeahead({
            source:  function (query, process) {
                return $.get(path, { query: query }, function (data) {
                    return process(data);
                });
            }
        });
    </script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/catclinic/resources/views/search.blade.php ENDPATH**/ ?>