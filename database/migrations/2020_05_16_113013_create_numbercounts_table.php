<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNumbercountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('numbercounts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('homepage_count');
            $table->integer('searchpage_count');
            $table->integer('queuepage_count');
            $table->integer('searchcount');
            $table->integer('queuecount');
            $table->integer('sum_count');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('numbercounts');
    }
}
