
@extends('layouts.app')
@section('content')
    <body style="background-color: #fdf7fa">

    <form method="post" action="{{ url('/home/'.$todo->id) }}">
        @csrf
        @method('PUT')
        <div class="container">
            <div style="padding: 30px;background: white;">
                    <div style="margin: 10px">
                        <a class="btn btn-primary active" href={{ url('/home/') }}><h5>ย้อนกลับ</h5> </a>
                    </div>


                <div class="row"style="background:#cae9ff; padding: 40px 0 20px  0; text-align: center;" >
                    <h5 style="margin:  0 auto">แก้ไขข้อมูล</h5>
                    <div class="input-group flex-nowrap" style="margin: 10px 300px 10px 300px ;" >
                        <input type="text" name="name_cat" class="form-control" placeholder="ชื่อแมว" value="{{ $todo->name_cat }}" >
                    </div>

                    <div class="input-group flex-nowrap" style="margin: 10px 300px 10px 300px ;" >
                        <input type="text" name="color_cat" class="form-control" placeholder="สี" value="{{ $todo->color_cat }}">
                    </div>

                    <div class="input-group flex-nowrap" style="margin: 10px 300px 10px 300px ;">
                        <select class="custom-select" name="sex" value="{{ $todo->sex }}">
                            <option selected>เลือกเพศ</option>
                            <option value="ผู้">ผู้</option>
                            <option value="เมีย">เมีย</option>

                        </select>
                    </div>

                    <div class="input-group flex-nowrap" style="margin: 10px 300px 10px 300px ;" >
                        <input type="text" name="service" class="form-control" placeholder="บริการ" aria-label="Username" aria-describedby="addon-wrapping"
                               value="{{ $todo->service }}">
                    </div>

                    <div class="" style="margin: 0 auto"  >
                        <button type="submit" class="btn btn-dark" >จองคิว</button>
                    </div>

                </div>

            </div>
        </div>

    </form>
    </body>
@endsection
