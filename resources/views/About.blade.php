@extends('layouts.app')
@section('content')
    <body style="background-color: #fdf7fa">

    <nav class="navbar navbar-expand-lg navbar-light " style="background-color: #cae9ff;color: white; height: 80px;margin:0 auto; width: 1138px">
        <a class="navbar-brand" href="#"> <img src="{{ url('uploads/LogoCat.png') }} "  style="width: 80px"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent" style="margin-left: 50px;">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="btn " href={{ url('/home/') }}><h5>หน้าแรก</h5> </a>
                </li>
                <li class="nav-item">
                    <a class="btn  nav-link" href={{ url('/home/queue/') }}><h5>จองคิว</h5></a>
                </li>

                <li class="nav-item">
                    <a class="btn  btn-primary active" href={{ url('/abouts') }}><h5>เกี่ยวกับเรา</h5></a>
                </li>
            </ul>

            <form method="get" action="{{ route('search') }}">

                <div class="container " style="margin: 10px 0 10px 0;">
                    <div class="input-group col align-self-end" style="float:right" >
                        <input type="text" class="form-control text-center "placeholder="ใส่คิวหรือชื่อแมวของคุณ"  name="search" value="">
                        <button class="btn btn-primary" type="submit">ค้นหา</button>
                    </div>
                </div>
            </form>
        </div>
    </nav>


    <div class="container "style="background: #fff;">
        <div class="row">
            <div class = "col-6" style="margin-top: 50px;">
                <h4>ข้อมูลการเข้าถึงเว็บไซต์</h4>
                @if(count($todos)>0)
                 @foreach($todos as $todo)
                <div class="row" style="color: #1b4965">

                                <div class="col-6" style="margin-top: 15px;">
                                    <h5 style="font-size: 20px">หน้าแรก</h5>
                                </div>
                                <div class="col-6">
                                    <h5>{{ $todo->homepage_count.' ครั้ง' }}</h5>
                                </div>

                            <div class="col-6" style="margin-top: 15px;">
                                <h5>จองคิว</h5>
                            </div>
                            <div class="col-6" style="margin-top: 15px;">
                                <h5>{{ $todo->queuepage_count.' ครั้ง'}}</h5>
                            </div>

                            <div class="col-6" style="margin-top: 15px;">
                                <h5>จำนวนการจองคิวทั้งหมด</h5>
                            </div>
                            <div class="col-6" style="margin-top: 15px;">
                                <h5>{{ $todo->searchcount.' ครั้ง'}}</h5>
                            </div>

                            <div class="col-6" style="margin-top: 15px;">
                                <h5>จำนวนการค้นหาทั้งหมด</h5>
                            </div>
                            <div class="col-6" style="margin-top: 15px;">
                                <h5>{{ $todo->queuecount.' ครั้ง' }}</h5>
                            </div>

                            <div class="col-6" style="margin-top: 15px;">
                                <h5>จำนวนการใช้งานเว็บ</h5>
                            </div>
                            <div class="col-6" style="margin-top: 15px;margin-bottom: 40px;">
                                <h5>{{ $todo->sum_count.' ครั้ง' }}</h5>
                            </div>

                 </div>
                @endforeach
                    @endif
                </div>
            <div class="col-6" style=" text-align: center;margin-top:20px;">
                <img  src="{{ url('uploads/123.jpg') }}"  class="img-fluid" width="200px" style="margin-top: 10px; border-radius: 100px;">

                <h5 style="margin-top: 10px;">Apichart Premjai 13600211</h5>
            </div>
             </div>


        </div>
    <div class="container" style="background : #1b4965 ;color: white; height: 50px; text-align: center ;padding: 10px;" >
        <div class="row ">
            <div class="col">
                <h5>โปรเจค 2562/2 เอกเว็บและสื่อโต้ตอบ นายอภิชาติ เปรมใจ</h5>
            </div>
        </div>
    </div>
    </body>
@endsection
