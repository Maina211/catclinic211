@extends('layouts.app')
@section('content')
{{--    <nav class="navbar navbar-expand-lg navbar-light " >--}}
{{--        <a class="navbar-brand" href="#" ><h1>Logo</h1></a>--}}
{{--        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"--}}
{{--                aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">--}}
{{--            <span class="navbar-toggler-icon"></span>--}}
{{--        </button>--}}
{{--        <div class="collapse navbar-collapse" id="navbarNavAltMarkup " >--}}
{{--            <div class="navbar-nav" style="margin-left: 300px; color: white">--}}

{{--                <a class="btn btn-primary active" href={{ url('/home/') }}><h5>หน้าแรก</h5> </a>--}}
{{--                <a class="btn  nav-link" href={{ url('/home/queue/') }}><h5>จองคิว</h5></a>--}}
{{--                <a class="btn  nav-link" href={{ url('/abouts') }}><h5>เกี่ยวกับเรา</h5></a>--}}

{{--            </div>--}}
{{--            <div style="margin-left: 350px;">--}}
{{--                @foreach($users as $todo)--}}
{{--                    <h5>{{ $todo->name }}</h5>--}}
{{--                @endforeach--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </nav>--}}


<body style="background-color: #fdf7fa">

    <nav class="navbar navbar-expand-lg navbar-light " style="background-color: #cae9ff;color: white; height: 80px ;margin:0 auto; width: 1138px">
        <a class="navbar-brand" href="#"> <img src="{{ url('uploads/LogoCat.png') }} "  style="width: 80px"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent" style="margin-left: 50px;">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="btn btn-primary active" href={{ url('/home/') }}><h5>หน้าแรก</h5> </a>
                </li>
                <li class="nav-item">
                    <a class="btn  nav-link" href={{ url('/home/queue/') }}><h5>จองคิว</h5></a>
                </li>

                <li class="nav-item">
                    <a class="btn  nav-link" href={{ url('/abouts') }}><h5>เกี่ยวกับเรา</h5></a>
                </li>
            </ul>

            <form method="get" action="{{ route('search') }}">

                <div class="container " style="margin: 10px 0 10px 0;">
                    <div class="input-group col align-self-end" style="float:right" >
                        <input type="text" class="form-control text-center "placeholder="ใส่คิวหรือชื่อแมวของคุณ"  name="search" value="">
                        <button class="btn btn-primary" type="submit">ค้นหา</button>
                    </div>
                </div>
            </form>
        </div>
    </nav>

{{--    nav--}}
    <div class="container" style="background-color:white">
        <div class="row">
            <div class="col">
                <img src="{{ url('uploads/cat1.jpg') }} "  style="width: 1000px">
            </div>
        </div>

    </div>
{{--    รูป--}}
    <div class="container" style=" background : #cae9ff; height: 400px;" >
        <div class="row ">
            <div class="col-12" style="margin-top: 20px;color: #1b4965;">
                <h3>รายการแนะนำ</h3>
            </div>

            <div class="col-4" >
                <div class="card" style="width: 18rem;text-align: center;">
                    <div class="card-body">
                        <img src="{{ url('uploads/cat2.jpg') }} "  style="width: 200px;height: 100px">
                        <p class="card-text"><h5>บริการตัดขน</h5>natural lead-in to additional content.</p>
                        <a href="#" class="btn btn-primary">อ่านเพิ่ม</a>
                    </div>
                </div>

            </div>
            <div class="col-4">
                <div class="card" style="width: 18rem;text-align: center;" >
                    <div class="card-body">
                        <img src="{{ url('uploads/cat3.jpg') }} "   style="width: 200px;height: 100px">
                        <p class="card-text"><h5>ฝากเลี้ยง</h5>natural lead-in to additional content.</p>
                        <a href="#" class="btn btn-primary">อ่านเพิ่ม</a>
                    </div>
                </div>

            </div>
            <div class="col-4">
                <div class="card" style="width: 18rem;text-align: center;">
                    <div class="card-body">
                        <img src="{{ url('uploads/cat4.jpg') }} "   style="width: 200px;height: 100px">
                        <p class="card-text"><h5>การรักษาโรคต่างๆ</h5>natural lead-in to additional content.</p>
                        <a href="#" class="btn btn-primary">อ่านเพิ่ม</a>
                    </div>
                </div>
            </div>
        </div>
    </div>



{{--    เนื้อหา--}}
    <div class="container" style="background : #ffffff;" >
        <div class="row">
            <div class="col-10" style= "margin-top: 20px">
                <h3>คิวการรักษา</h3>
            </div>
            <div class="col-2" style="margin-top: 25px;float: right">
                <h5>5/10/2562</h5>
            </div>
        </div>

        <table class="table"  style="margin-top: 10px">
            <thead class=""style="background-color: #cae9ff;x">
            <tr>
                <th scope="col">คิว</th>
                <th scope="col">รูป</th>
                <th scope="col">ขื่อแมว</th>
                <th scope="col">สี</th>
                <th scope="col">เพศ</th>
                <th scope="col">บริการ</th>
                <th scope="col" width="200px"></th>
            </tr>
            </thead>

            <tbody>


                @foreach($todos as $todo)
                    <tbody>
                        <tr>
                            <td>  <p>{{ $todo->id }}</p></td>
                            <td><img src="{{ url('uploads/'.$todo->file_name) }}" width="120"></td>
                            <td ><p>{{ $todo->name_cat }}</p></td>
                            <td ><p>{{ $todo->color_cat }}</p></td>
                            <td ><p>{{ $todo->sex }}</p></td>
                            <td ><p>{{ $todo->service }}</p></td>

                            <td>

                                <form action="{{url('/home/'.$todo->id) }}" method="post" id="form-delete">
                                    @method('DELETE')
                                    @csrf
                                 <button class="btn btn-danger" onclick="confirm_delete1()" type="button">Delete</button>
                                    <a href="{{url('/home/'.$todo->id.'/edit')}}" class="btn btn-primary">Edit</a>
                                </form>


                            </td>
                        </tr>

                    </tbody>

                @endforeach

        </table>
    </div>

    <div class="container" style="background : #1b4965 ;color: white; height: 50px; text-align: center ;padding: 10px;" >
        <div class="row ">
            <div class="col-10">
                <h5>โปรเจค 2562/2 เอกเว็บและสื่อโต้ตอบ นายอภิชาติ เปรมใจ</h5>
            </div>


        </div>
    </div>
    <script>
        function confirm_delete1() {
            var text = '{!! $todo->title !!}';
            //ต้องใส่ !! เพราะไม่แน่ใจว่ามันเป็น textรึปล่า
            var confirm = window.confirm('ยืนยันการลบ'+text);
            if (confirm){
                document.getElementById('form-delete').submit();
            }
        }

    </script>

</body>
@endsection

