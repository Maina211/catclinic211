@extends('layouts.app')
@section('content')
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#" ><h1>Logo</h1></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup " >
            <div class="navbar-nav" style="margin-left: 300px;">

                <a class="nav-item nav-link active" href={{ url('/home/') }}><h5>หน้าแรก</h5> <span class="sr-only">(current)</span></a>
                <a class="nav-item nav-link" href={{ url('/home/queue/') }}><h5>จองคิว</h5></a>
                <a class="nav-item nav-link" href="http://www.catclinic.local/search"><h5>ค้นหา</h5></a>
                <a class="nav-item nav-link" href={{ url('/abouts') }}><h5>เกี่ยวกับเรา</h5></a>
            </div>
        </div>
    </nav>
    {{--    ตัวหลัก--}}
{{--    <form action="/search" method="get">--}}
{{--        <div class="container" >--}}

{{--            <div style="padding: 30px;">--}}
{{--                <div class="row"style="background: #999999; padding: 50px 0 50px  0; text-align: center;" >--}}
{{--                    <h5 style="margin:  0 auto">ค้นหา</h5>--}}


{{--                            <div class="input-group flex-nowrap" style="margin: 5px 300px 5px 300px ;" >--}}
{{--                                <input  type="search" name="search" placeholder="ใส่คิวหรือชื่อแมว" aria-label="Username" aria-describedby="addon-wrapping " style="width: 400px;">--}}
{{--                                <span class="from-group-btn">--}}
{{--                                        <button type="" class="btn btn-primary">  href={{ url('/searchtext') }}><h5>จองคิว</h5></button>--}}
{{--                                    <button class="btn btn-primary"> <a style="color: white;font-size: 16px" href={{ url('/searchtext') }}>ค้นหา</a></button>--}}
{{--                                </span>--}}
{{--                            </div>--}}


{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </form>--}}
    <form method="get" action="{{ route('search') }}">

        <div class="container">
            <div class="input-group col align-self-end">
                <input type="text" class="form-control text-center "placeholder="......"  name="search" value="">
                <button class="btn btn-primary" type="submit">ค้นหา</button>
            </div>
        </div>
    </form>



    <div class="container" style="background : #1b4965 ;color: white; height: 50px; text-align: center ;padding: 10px;" >
        <div class="row ">
            <div class="col">
                <h5>โปรเจค 2562/2 เอกเว็บและสื่อโต้ตอบ นายอภิชาติ เปรมใจ</h5>
            </div>
        </div>
    </div>

@endsection
