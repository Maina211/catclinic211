@extends('layouts.app')
@section('content')
    <body style="background-color: #fdf7fa">
    <nav class="navbar navbar-expand-lg navbar-light " style="background-color: #cae9ff;color: white; height: 80px;margin:0 auto; width: 1138px">
        <a class="navbar-brand" href="#"> <img src="{{ url('uploads/LogoCat.png') }} "  style="width: 80px"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent" style="margin-left: 50px;">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="btn  " href={{ url('/home/') }}><h5>หน้าแรก</h5> </a>
                </li>
                <li class="nav-item">
                    <a class="btn  btn-primary active" href={{ url('/home/queue/') }}><h5>จองคิว</h5></a>
                </li>

                <li class="nav-item">
                    <a class="btn  nav-link" href={{ url('/abouts') }}><h5>เกี่ยวกับเรา</h5></a>
                </li>
            </ul>

        </div>
    </nav>
    <form method="post" action="{{ url('/home/') }}" enctype="multipart/form-data">
        @csrf
        <div class="container" style="background:white">
            <div style="padding: 30px;">

                <div class="row"style="background: #cae9ff; padding: 20px 0 20px  0; text-align: center;color: #1b4965;"  >
                    <h4 style="margin:  0 auto">จองคิวของคุณ</h4>
                    <div class="input-group flex-nowrap" style="margin: 10px 300px 10px 300px ;" >
                        <input type="text" name="name_cat" class="form-control" placeholder="ชื่อแมว" >
                     </div>

                    <div class="form-group flex-nowrap " style="margin: 10px 300px 10px 300px ;width: 500px">
                        <input type="file" name="file" class="form-control" >
                    </div>
                    <div class="input-group flex-nowrap" style="margin: 10px 300px 10px 300px ;" >
                        <input type="text" name="color_cat" class="form-control" placeholder="สี">
                    </div>
                    <div class="input-group flex-nowrap" style="margin: 10px 300px 10px 300px ;" >
                        <select class="custom-select" name="sex">
                            <option selected>เลือกเพศ</option>
                            <option value="ผู้">ผู้</option>
                            <option value="เมีย">เมีย</option>

                        </select>
                    </div>
                    <div class="input-group flex-nowrap" style="margin: 10px 300px 10px 300px ;" >
                        <input type="text" name="service" class="form-control" placeholder="บริการ" aria-label="Username" aria-describedby="addon-wrapping">
                    </div>

                    <div class="" style="margin: 0 auto"  >
                        <button type="submit" class="btn btn-dark" >จองคิว</button>
                    </div>

                </div>

            </div>
        </div>

    </form>

    <div class="container" style="background : #1b4965 ;color: white; height: 50px; text-align: center ;padding: 10px;" >
        <div class="row ">
            <div class="col">
                <h5>โปรเจค 2562/2 เอกเว็บและสื่อโต้ตอบ นายอภิชาติ เปรมใจ</h5>
            </div>
        </div>
    </div>
    </body>
@endsection
