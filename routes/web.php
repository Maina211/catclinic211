<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
////Route for normal user
Route::group(['middleware' => ['auth']], function () {
    //Route::get('/', 'HomeController@index');
    Route::get('/', 'HomeController@index');
//    Route::get('/','TodosController@index');
//    Route::get('/abouts','TodosController@aboutit');
//    Route::resource('/home','TodosController');
//    Route::any('/search','TodosController@search')->name('search');
//    Auth::routes();

});
//Route for admin
Route::group(['prefix' => 'admin'], function(){
    Route::group(['middleware' => ['admin']], function(){
        Route::get('/Login211', 'admin\AdminController@index');
    });
});
    Route::get('/','TodosController@index');
    Route::get('/abouts','TodosController@aboutit');
    Route::resource('/home','TodosController');
    Route::any('/search','TodosController@search')->name('search');
    Auth::routes();




